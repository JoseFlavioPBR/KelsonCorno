package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import entidades.T;

public class AbstractDao<T> {

	static EntityManagerFactory fac = Persistence.createEntityManagerFactory("T");

	public void salvar(T entity) {
		EntityManager manager = fac.createEntityManager();
		manager.getTransaction().begin();
		manager.persist(entity);
		manager.getTransaction().commit();
		manager.close();
	}

	public void atualizar(T entity) {
		EntityManager manager = fac.createEntityManager();
		manager.getTransaction().begin();
		manager.merge(entity);
		manager.getTransaction().commit();
		manager.close();
	}


	public void remover(T T) {
		EntityManager manager = fac.createEntityManager();
		manager.getTransaction().begin();
		T = manager.find(T.class, entity.getId());
		manager.persist(T);
		manager.getTransaction().commit();
		manager.close();
	}


	public T buscaId(Long id) {
		EntityManager manager = fac.createEntityManager();
		T entity = manager.find(T.class, id);
		manager.close();
		return entity;
	}

	public List<T> listar() {
		EntityManager manager = fac.createEntityManager();
		Query query = manager.createQuery("select c from T c");
		List<T> T = query.getResultList();
		manager.close();
		return T;
	}

}
