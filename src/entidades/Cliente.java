package entidades;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.List;

@Entity
@SequenceGenerator(sequenceName="cliente_seq", name="cliente_id")
public class Cliente {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator="cliente_id")
	private Long id;
	private String nome;
	@Temporal(TemporalType.TIMESTAMP)
	private String dataInicio;
	@Temporal(TemporalType.TIMESTAMP)
	private String dataFim;
	
	@ManyToMany
	private List<Conta> contas;
	
	@OneToMany(mappedBy="cliente")
	private List<Endereco> endereco;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}
	public String getDataFim() {
		return dataFim;
	}
	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}	
}
