package entidades;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import java.util.List;



@Entity
@SequenceGenerator(sequenceName="conta_seq", name="conta_id")
public class Conta {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator="conta_id")
	private long id;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataAbertura;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataFechamento;
	private Double creditoBloqueado;
	private Double debitoBloqueado;
	
	@OneToMany(mappedBy="conta")
	private List <Movimento> movimento;
	
	@ManyToMany
	private List <Cliente> clientes;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Date getDataAbertura() {
		return dataAbertura;
	}
	public void setDataAbertura(Date dataAbertura) {
		this.dataAbertura = dataAbertura;
	}
	public Date getDataFechamento() {
		return dataFechamento;
	}
	public void setDataFechamento(Date dataFechamento) {
		this.dataFechamento = dataFechamento;
	}
	public Double getCreditoBloqueado() {
		return creditoBloqueado;
	}
	public void setCreditoBloqueado(Double creditoBloqueado) {
		this.creditoBloqueado = creditoBloqueado;
	}
	public Double getDebitoBloqueado() {
		return debitoBloqueado;
	}
	public void setDebitoBloqueado(Double debitoBloqueado) {
		this.debitoBloqueado = debitoBloqueado;
	}
	
	
	
}
